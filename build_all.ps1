﻿[System.Environment]::SetEnvironmentVariable("PSModulePath", $env:PSModulePath + ";C:\Program Files (x86)\PowerShell Community Extensions\Pscx3\")
#Import-Module pscx
#if (-Not (Get-Command 'Invoke-BatchFile' -errorAction SilentlyContinue))
#{
#    
    #(new-object Net.WebClient).DownloadString("http://psget.net/GetPsGet.ps1") | iex
    #Import-Module PsGet
    #Install-Module pscx
#}
Import-Module pscx

function compile($path, $cmake_generator, $make_command)
{
    $oldpath = "$pwd"
    mkdir $path
    cd $path
    cmake -G "$cmake_generator" ..\..
    Invoke-Expression "$make_command"

    cd $oldpath
}

echo "*** Setting up PATH ..."
$old_path = $env:Path
Invoke-BatchFile 'C:\Program Files (x86)\Microsoft Visual Studio 12.0\Common7\Tools\VsDevCmd.bat'

$build_dir = "temp_build_dir"
if (Test-Path $build_dir)
{
    echo "*** Build dir $build_dir already exist, cleaning up..."
    Remove-Item ".\$build_dir" -Force -Recurse | Out-Null
}

echo "*** Creating build dir..."
(mkdir $build_dir) | Out-Null
cd $build_dir

echo "*** Cloning deps..."
hg clone https://bitbucket.org/cegui/cegui-dependencies

echo "*** Building deps..."
cd cegui-dependencies
compile "build\mingw" "MinGW Makefiles" "mingw32-make"

cd ..

echo "*** Cloning cegui..."
#hg clone https://bitbucket.org/cegui/cegui
cd ..

echo "*** Restoring PATH..."
$env:Path = $old_path