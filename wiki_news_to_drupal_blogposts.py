import urllib2, HTMLParser, xml.dom.minidom, sys, time, datetime
from xml.dom.minidom import parseString, parse, Node

html_escape_table = {
    "&": "&amp;",
     '"': "&quot;",
     "'": "&apos;",
     ">": "&gt;",
     "<": "&lt;",
    }
 
def html_escape(text):
    """Produce entities within text."""
    return "".join(html_escape_table.get(c,c) for c in text)

def getText(the_node):
    rc = []
    for node in the_node.childNodes:
        if node.nodeType == node.TEXT_NODE:
            rc.append(node.data)
    return ''.join(rc)

def getWikiNews():
    wikiNews = urllib2.urlopen("http://cegui.org.uk/wiki/index.php?title=News&action=edit")
    parsedWiki = parse(wikiNews)
    return getText(parsedWiki.getElementsByTagName("textarea")[0])

def resolveMediaWikiUrl(mediaWikiUrl):
    tokens = mediaWikiUrl.split("|", 1)
    if len(tokens) == 1:
        # try with space (for simple urls, outside mediawiki)
        tokens = mediaWikiUrl.split(" ", 1)

    url = tokens[0]
    value = tokens[1] if len(tokens) >= 2 else url

    realUrl = url if url.startswith("http") else 'http://cegui.org.uk/wiki/' + url.replace(" ", "_")

    return '<a href="' + realUrl + '">' + value + '</a>'

def transformMediaWikiTags(text):
    newText_list = []
    index = 0;
    while index < len(text):
        bracketsCount = 1
        startIndex = text.find("[", index)
        if startIndex == -1:
            break;

        # double brackets
        if text[startIndex + 1] == '[':
            bracketsCount += 1

        # skip the brackets
        startIndex += bracketsCount
        endIndex = text.find("]", startIndex)
        # double brackets
        if text[endIndex + 1] == ']':
            endIndex += 1
        stringToParse = text[startIndex:endIndex]

        newText_list.append(text[index:startIndex - bracketsCount])
        newText_list.append(resolveMediaWikiUrl(stringToParse))

        index = endIndex + bracketsCount;

    # add final bits
    newText_list.append(text[index:len(text)])

    return ''.join(newText_list);

nodesXml = ""
wikiNews = getWikiNews()
index = 0
itemId = 18
with open("template.xml", "r") as file:
    template = file.read()
#template = sys.stdin.read
    while index < len(wikiNews):
        blogPost = []
        startIndex = wikiNews.find("===", index)
        endIndex = wikiNews.find("===", startIndex + 3)
        if startIndex == -1 or endIndex == -1:
            break;

        newsHeader = wikiNews[startIndex + 4:endIndex - 1]
        index = endIndex + 4

        byIndex = newsHeader.rfind("by")
        if byIndex == -1:
            continue
        userStartIndex = newsHeader.rfind("[[User:") + 7
        userEndIndex = newsHeader.find("|", userStartIndex)
        dateIndex = newsHeader.rfind("]]") + 3

        title = transformMediaWikiTags(newsHeader[:byIndex - 1])
        user = transformMediaWikiTags(newsHeader[userStartIndex:userEndIndex])
        dateString = newsHeader[dateIndex:]
        firstSpaceIndex = dateString.find(" ")
        # in the first token (the day) remove the offenders :D
        dateString = dateString[:firstSpaceIndex].replace("rd", "").replace("st", "").replace("th", "").replace("nd", "") + dateString[firstSpaceIndex:]
        date = datetime.datetime.strptime(dateString, "%d %B %Y")
        newsTime = int(time.mktime(date.timetuple()))
        #print "TITLE: (", title , "), USER: (", user , "), Date: (", dateString ,")"
        #print newsTime

        nextIndex = wikiNews.find("===", index)
        contents = wikiNews[index:nextIndex]

        index = nextIndex;
        contents = transformMediaWikiTags(contents)
        escapedContents = html_escape(contents)
        # print "CONTENTS: ", contents

        nodesXml += template.format(itemId = itemId, title = title, value = escapedContents, itemUrlTitle = "imported"  + str(itemId), timestamp = newsTime)
        itemId += 1

with open("out.xml", "w") as outFile:
    outFile.write('<?xml version="1.0" encoding="UTF-8" ?>\n<node_export created="Mon, 20 Jan 2014 19:36:48 +0100">\n' + nodesXml + '\n</node_export>')
#print '<?xml version="1.0" encoding="UTF-8" ?>\n<node_export created="Mon, 20 Jan 2014 19:36:48 +0100">\n', nodesXml, '\n</node_export>'
