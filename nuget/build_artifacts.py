#!/usr/bin/env python2
from distutils import dir_util, spawn
from itertools import chain
import subprocess
import shutil
import os
import zipfile
import time


def setupPath(path):
    if os.path.isdir(path):
        print "*** Cleaning up", path, "..."
        shutil.rmtree(path)

    if not os.path.exists(path):
        os.mkdir(path)


def generateMSBuildCommand(filename, configuration):
    return ["msbuild", filename, "/p:Configuration=" + configuration]


def getMSVCCompiler(x):
    return ('msvc' + str(x),
            "Visual Studio " + (str(x) if x > 9 else '9 2008'),
            list(chain.from_iterable((generateMSBuildCommand("src/pcre-8.12/CEGUI-BUILD/dftables." + ("vcxproj" if x > 9 else "vcproj"), config),
                                      generateMSBuildCommand("CEGUI-DEPS.sln", config)) for config in ["RelWithDebInfo", "Debug"])))


def getCompilers():
    return [
        item for sublist in
        [
            #TODO: add RelWithDebInfo for mingw also
            [('mingw', 'MinGW Makefiles', [['mingw32-make', 'dftables'], ['mingw32-make']])],
            [getMSVCCompiler(x) for x in list(xrange(9, 13))]
        ]
        for item in sublist
    ]


def invokeCMake2(ceguiSourceDir, depsDir, msvcVersion):
    cmakeCmd = ["cmake",
                "-G", "Visual Studio " + msvcVersion,
                "-DCMAKE_PREFIX_PATH=" + depsDir,
                "-DCEGUI_SAMPLES_ENABLED=FALSE",
                "-DCEGUI_BUILD_LUA_GENERATOR=FALSE",
                "-DCEGUI_BUILD_LUA_MODULE=FALSE",
                ceguiSourceDir]
    print "***Invoking CMake ", cmakeCmd, "..."
    cmakeProc = subprocess.Popen(cmakeCmd)
    cmakeProc.wait()
    print "*** CMake generation return code: ", cmakeProc.returncode


def invokeMSBuild2(buildDir, configuration):
    msbuildCmd = ["msbuild", "cegui.sln", "/p:Configuration=" + configuration]
    print "*** Compiling", msbuildCmd, "..."
    msbuildProc = subprocess.Popen(msbuildCmd, cwd=buildDir)
    msbuildProc.wait()
    print "*** MSBuild compilation return code:", msbuildProc.returncode


def buildCEGUI(ceguiSourceDir, ceguiDepsPrefix):
    if not os.path.isdir(ceguiSourceDir):
        print "*** Invalid CEGUI source dir"
        return

    print "*** Using source: [", ceguiSourceDir, "] and deps prefix: [", ceguiDepsPrefix, "]"

    for msvcVersion in ("10", "11", "12"):
        buildDir = os.getcwd() + "\\build_msvc" + msvcVersion
        depsDir = ceguiDepsPrefix + msvcVersion

        if not os.path.isdir(depsDir):
            print("*** No deps dir for msvc", msvcVersion)
            continue

        setupPath(buildDir)
        invokeCMake2(buildDir, ceguiSourceDir, depsDir, msvcVersion)
        invokeMSBuild2(buildDir, "RelWithDebInfo")
        invokeMSBuild2(buildDir, "Debug")


def clone(url, target):
    print "*** Cloning from '", url, "' to '", target, "'..."
    hg = subprocess.Popen(["hg", "clone", url, target]).wait()


def invokeCMake(sourceDir, generator):
    cmakeCmd = ["cmake",
                "-G", generator,
                sourceDir]
    print "*** Invoking CMake ", cmakeCmd, "..."
    cmakeProc = subprocess.Popen(cmakeCmd).wait()
    print "*** CMake generation return code: ", cmakeProc


def makeZip(source, zip):
    zipFile = zipfile.ZipFile(zip, 'w')

    for root, dirs, files in os.walk(source):
        for file in files:
            zipFile.write(os.path.join(root, file))

    zipFile.close()


def gatherDeps(compiler, artifactsGatherPath):
    print "*** Gathering artifacts for CEGUI deps for '%s' compiler ..." % compiler
    if not os.path.isdir("dependencies"):
        print "*** ERROR: no dependencies directory found, nothing generated!"
        return

    artifactDirName = "dependencies_" + compiler
    artifactZipName = artifactDirName + ".zip"

    makeZip("dependencies", artifactZipName)
    dir_util.copy_tree("dependencies", os.path.join(artifactsGatherPath, artifactDirName))
    shutil.copyfile(artifactZipName, os.path.join(artifactsGatherPath, artifactZipName))


def buildCEGUIDeps(depsDir, artifactsGatherPath):
    old_path = os.getcwd()
    os.chdir(depsDir)

    print "*** Building CEGUI deps ..."

    for (compiler, generator, commands) in getCompilers():
        print "\n*** Using '%s' compiler..." % compiler
        buildDir = os.path.join(depsDir, "build" + compiler)
        setupPath(buildDir)
        os.chdir(buildDir)

        invokeCMake(depsDir, generator)
        for command in commands:
            print "*** Executing compiler command: ", command
            cmd = subprocess.Popen(command).wait()

        gatherDeps(compiler, artifactsGatherPath)

    os.chdir(old_path)


def ensureCanBuild():
    def has_exe(name):
        if spawn.find_executable(name) is None:
            print "No program named '%s' could be found on PATH! Aborting... " % name
            exit(1)
        return True

    has_exe('msbuild')
    has_exe('cmake')
    has_exe('mingw32-make')


def main():
    ensureCanBuild()

    curPath = os.path.abspath(os.path.join(os.path.dirname(__file__)))
    artifactsPath = os.path.join(curPath, "artifacts")
    if not os.path.exists(artifactsPath):
        os.mkdir(artifactsPath)

    tempPath = os.path.join(curPath, "local-temp")
    setupPath(tempPath)

    startTime = time.time()
    clone('https://bitbucket.org/cegui/cegui-dependencies', os.path.join(tempPath, 'cegui-dependencies'))
    buildCEGUIDeps(os.path.join(tempPath, 'cegui-dependencies'), artifactsPath)
    print "!!!!! CEGUI Dependencies total build time: ", (time.time() - startTime), " seconds."
    #clone('https://bitbucket.org/cegui/cegui', 'cegui')
    #if len(sys.argv) < 3:
    #        print "Need two parameters: <cegui source dir> <cegui dependencies dir prefix>"
    #        exit(1)

    #buildCEGUI(sys.argv[1], sys.argv[2])



if __name__ == "__main__":
    main()